# Box Model

When laying out a document, the browser's rendering engine represents each element as a rectangular box according to the standard [CSS basic box model](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Model/Introduction_to_the_CSS_box_model).

## Parts of a box

Every box is composed of four parts (or areas):

- **Content box**: The area where your content is displayed.

- **Padding box**: The padding sits around the content as white space.

- **Border box**: The border box wraps the content and any padding.

- **Margin box**: The margin is the outermost layer, wrapping the content, padding, and border as whitespace between this box and other elements.

The below diagram shows these layers:

![boxModel](../assets/03-boxModel/boxModel.png)

## Exercise

Lets get familiar with the CSS box model

<Codepen
    title="Box Model"
    user="carolineartz"
    slug="ogVXZj"
    :css="false"
/>



