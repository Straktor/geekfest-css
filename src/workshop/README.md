# Introduction

The separation of document structure from the document's layout had been a goal of HTML from its inception in 1990.

A document structure is define by a [Markup Language](https://en.wikipedia.org/wiki/Markup_language) such as HTML, XHTML and XML.

A document presentation is defined by stylesheets such as CSS.

## Life Before Stylesheets

Here is what the internet used to look like ([Emulator of the first browser](https://line-mode.cern.ch/www/hypertext/WWW/TheProject.html))


![Emulator](../assets/00-introduction/emulator.jpg)

## History by Dates
| Version  | Date       | Description                                                                                |
|----------|------------|--------------------------------------------------------------------------------------------|
| HTML 1.0 | 1993       | Intention of sharing information that can be readable and accessible via web browsers      |
| HTML 2.0 | 1995       | Contains all the features of HTML 1.0 along with that few additional features              |
| CSS 1    | 1996       | More powerful characteristics for webmasters in designing web pages                        |
| HTML 3.0 | 1997       | New features, giving more powerful characteristics for webmasters. Slowed down the browser |
| CSS 2    | 1998       | Essentially a superset of CSS 1, suffered from a number of errors                          |
| HTML 4.0 | 1999       | The widely-used, very successful                                                           |
| CSS 3    | 1999-Today | Instead of a single monolithic specification, it was published as [modules](https://en.wikipedia.org/wiki/CSS#CSS_3) |
| CSS 2.1  | 2011       | Fixed the errors found in CSS 2                                                            |
| HTML 5.0 | 2014       | Used worldwide. Extended version of HTML 4.00                                              |


## CSS Mission
CSS is the language for describing the presentation of Web pages, including colors, layout, and fonts.
It allows one to adapt the presentation to different types of devices, such as large screens, small screens, or printers.

The separation of HTML from CSS makes it easier to maintain sites, share style sheets across pages, and tailor pages to different environments.
([w3.org](https://www.w3.org/standards/webdesign/htmlcss.html))
  
## Cascading Style Sheets

The CSS cascade algorithm's job is to select CSS declarations in order to determine the correct values for CSS properties. CSS declarations come from different origins: the [User-agent stylesheets](https://developer.mozilla.org/en-US/docs/Web/CSS/Cascade#user-agent_stylesheets), the [Author stylesheets](https://developer.mozilla.org/en-US/docs/Web/CSS/Cascade#author_stylesheets), and the [User stylesheets](https://developer.mozilla.org/en-US/docs/Web/CSS/Cascade#user_stylesheets).

Though style sheets come from these different origins, they overlap in scope; to make this work, the cascade algorithm defines how they interact.

![cssCascade](../assets/00-introduction/cssCascade.jpg)

## Overdefining

Knowing that the resulting CSS is a cascade from the **user's stylesheet**, **author's stylesheet** and **system stylesheet** it is important that when we write CSS we try not to overdefine every property.  We should "suggest" to the browser how we would like it to be displayed.  This usually supports a larger range of clients (smartphones, email, browsers...).

![designControlSuggest](../assets/00-introduction/designControlSuggest.png)

Its important to remember that the default CSS properties ensure that all the content of the markup language is shown and that the page is responsive.  This means that when a webpage stops working on a certain device it is usually due to some CSS properties that were added.

## Exercise 

Try playing around with the overflow property

```css
overflow: visible|hidden|scroll|auto|initial|inherit;
```

You can also try to modify or remove the width

<Codepen
    title="CSS IS AWESOME"
    user="miriamsuzanne"
    slug="JgKXeL"
    :css="false"
/>

> This exercise has been taken from this [video](https://www.youtube.com/watch?v=aHUtMbJw8iA) by [Miriam Suzanne](https://twitter.com/TerribleMia) it does a great job explaining the challenges of CSS.
