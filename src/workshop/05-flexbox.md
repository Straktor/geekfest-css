# Flexbox

[Flexbox](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Flexbox) is a one-dimensional layout method for arranging items in rows or columns.

[CSS Tricks](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) as a really great visual documentation on Flexbox.

```CSS
.container {
    display: flex;
}
```
### Container
| Property        | Description                                              |
| --------------- | -------------------------------------------------------- |
| display         | Enables flex for all children                            |
| flex-direction  | Establishes the main axis                                |
| flex-wrap       | Wraps items if they can't all be made to fit on one line |
| justify-content | Attempts to distribute extra space on the main axis      |
| align-items     | Determines how items are laid out on the cross axis      |

### Children
| Property    | Description                                                      |
| ----------- | ---------------------------------------------------------------- |
| order       | set the order you want each child to appear in                   |
| flex-grow   | determine how each child is allowed to grow as a part of a whole |
| flex-basis  | size of an element before remaining space is distributed         |
| flex-shrink | determine how each child is allowed to grow as a part of a whole |
| align-self  | sets alignment for individual item                               |


## Exercise

Lets get familiar with Flexbox.

<Codepen
    title="Flexbox"
    user="Straktor"
    slug="VwWqLXV"
    :css="false"
/>
