# Specificity

[Specificity](https://developer.mozilla.org/en-US/docs/Web/CSS/Specificity) is the means by which browsers decide which CSS property values are the most relevant to an element and, therefore, will be applied. Specificity is based on the matching rules which are composed of different sorts of CSS selectors.

## Calculating specificity 

The following list of selector types increases by specificity:
- [Type selectors](https://developer.mozilla.org/en-US/docs/Web/CSS/Type_selectors)
- [Class selectors](https://developer.mozilla.org/en-US/docs/Web/CSS/Class_selectors)
- [ID selectors](https://developer.mozilla.org/en-US/docs/Web/CSS/ID_selectors)


| Selector             | Inline | ID | Class | Type | Total specificity |
|----------------------|--------|----|-------|------|-------------------|
| h1                   |      0 |  0 |     0 |    1 |              0001 |
| h1 > p::first-letter |      0 |  0 |     0 |    3 |              0003 |
| .container           |      0 |  0 |     1 |    0 |              0010 |
| #identifier	       |      0 |  1 |     0 |    0 |              0100 |
| #outer #inner a	   |      0 |  2 |     0 |    1 |              0201 |
| Inline CSS	       |      1 |  0 |     0 |    0 |              1000 |

There are many [Specificity Calculator](https://specificity.keegan.st/) to help visually understand CSS specificity.

## Inheritance

In CSS, [inheritance](https://developer.mozilla.org/en-US/docs/Web/CSS/inheritance) controls what happens when no value is specified for a property on an element.

CSS properties can be categorized in two types:

- **inherited properties**
- **non-inherited properties**

### Inherited properties
A typical example of an inherited property is the [color](https://developer.mozilla.org/en-US/docs/Web/CSS/color) property.

```CSS
p { color: green; }
```

```html
<p>This paragraph has <em>emphasized text</em> in it.</p>
```

The words `emphasized text` will appear green, since the `em` element has inherited the value of the color property from the `p` element.

### Non-inherited properties
A typical example of a non-inherited property is the [border](https://developer.mozilla.org/en-US/docs/Web/CSS/border) property

```CSS
p { border: medium solid; }
```

```html
<p>This paragraph has <em>emphasized text</em> in it.</p>
```

the words `emphasized text` will not have a border 

## Exercise

Lets get familiar with the specificity.

<Codepen
    title="Box Model"
    user="Straktor"
    slug="GREPgjM"
    :css="false"
/>