# What's next?

## [Developer tools](https://css-tricks.com/devtools-for-designers/)
![Developer tools](../assets/06-whats-next/devtools.png)

## [CSS Grid](https://css-tricks.com/snippets/css/complete-guide-grid/)
![CSS Grid](../assets/06-whats-next/cssgrid.png)

### [CSS Battle](https://cssbattle.dev/)
![CSS Battle](../assets/06-whats-next/cssbattle.png)

## [1 line layout](https://1linelayouts.glitch.me/)
![1 line layout](../assets/06-whats-next/1linelayout.png)

## [Pseudo-classes vs Pseudo-elements](https://www.youtube.com/watch?v=RmDh3m8b9cU)
![Pseudo-classes vs Pseudo-elements](../assets/06-whats-next/pseudo.png)
