# Syntax

In order to write CSS it is important to understand the [syntax](https://developer.mozilla.org/en-US/docs/Web/CSS/Syntax) and its structure.

```css
selectors {
    property: value;
}
```

## CSS declaration
- A property and value pair is called a declaration.
- Both properties and values are case-insensitive
- Each selector is separated by `,`

![cssDeclaration](../assets/01-syntax/cssDeclaration.png)
![groupOfSelectors](../assets/01-syntax/groupOfSelectors.png)


## Properties

The properties are identifiers, that are human-readable names, that defines a feature (borders, margins, colors...) of and element. There are more than [100 different properties](https://developer.mozilla.org/en-US/docs/Web/CSS/Reference) in CSS and a nearly infinite number of different values.

### List of properties

| Property                                                                              | Description | Example  |
| ------------------------------------------------------------------------------------- | ----------- | -------- |
| [font](https://developer.mozilla.org/en-US/docs/Web/CSS/font)                         | [shorthand property](https://developer.mozilla.org/en-US/docs/Web/CSS/Shorthand_properties) to set an element's font | `font: italic 1.2em "Fira Sans", serif;` |
| [color](https://developer.mozilla.org/en-US/docs/Web/CSS/color)                       | set the foreground color | `color: red;` |
| [background-color](https://developer.mozilla.org/en-US/docs/Web/CSS/background-color) | set the background color of an element | `background-color: blue;` |
| [margin](https://developer.mozilla.org/en-US/docs/Web/CSS/margin)                     | set an element's margin | `margin: 1em` |
| [border](https://developer.mozilla.org/en-US/docs/Web/CSS/border)                     | [shorthand property](https://developer.mozilla.org/en-US/docs/Web/CSS/Shorthand_properties) to set an element's border | `border: 1px dashed black;` |
| [padding](https://developer.mozilla.org/en-US/docs/Web/CSS/padding)                   | set an element's padding | `padding: 1em;` |
| [...](https://developer.mozilla.org/en-US/docs/Web/CSS/Reference)                   |  |  |

## Selectors
[CSS selectors](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors) define the elements that a CSS declaration block impacts.

| Selector              | Characters    | Example           |
| --------------------- | ------------- | ----------------- |
| Universal             | `*`           | `*`               |
| Type                  | `elementname` | `p`               |
| Class                 | `.classname`  | `.title`          |
| Id                    | `#id`         | `#title`          |
| List                  | `,`           | `h1, h2`          |
| Descendant combinator | ` `(space)    | `p span`          |
| Child combinator      | `>`           | `p > span`        |
| Pseudo classe         | `:`           | `h1:hover`        |
| Pseudo element        | `::`          | `p::first-letter` |
| [...](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors) |  |  |

## Exercise

Test your skills with the [CSS selector cheatsheet](https://frontend30.com/css-selectors-cheatsheet) created by [nana](https://nanajeon.com/) and [Ryan Yu](https://ryanyu.com/).


![Exercise](../assets/01-syntax/exercise.png)

Once completed download the [CSS selector cheetsheet](https://www.dropbox.com/s/h2hni9o1m1di989/CSS%20selectors%20cheatsheet.pdf?dl=1) for later reference.