# Units

## Absolute length units

The most popular [units](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Values_and_units#lengths) when starting with CSS is usually the Pixel. It is simple to understand but it sometimes lack a way to adapt when the canvas changes.

| Unit | Name   |
|------|--------|
| px   | Pixels |
| ...  |        |

## Relative units

Relative [units](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Values_and_units#lengths) are a great way to define your components in a way that will allow the browser to adapt your design based on your users canvas.

| Unit | Relative to                                                         |
|------|---------------------------------------------------------------------|
| em   | Font size of the parent, in the case of typographical properties like font-size, and font size of the element itself, in the case of other properties like width. |
| ex   | x-height of the element's font.                                     |
| ch   | The advance measure (width) of the glyph "0" of the element's font. |
| rem  | Font size of the root element.                                      |
| lh   | Line height of the element.                                         |
| vw   | 1% of the viewport's width.                                         |
| vh   | 1% of the viewport's height.                                        |
| vmin | 1% of the viewport's smaller dimension.                             |
| vmax | 1% of the viewport's larger dimension.                              |

## Exercise

Lets get familiar with the different units available.

<Codepen
    title="Units"
    user="Straktor"
    slug="powQYRa"
    :css="false"
/>

