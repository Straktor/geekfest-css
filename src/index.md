---
home: true
heroImage: /geekfest.png
tagline: 
actionText: Begin →
actionLink: /workshop/
features:
- title: CSS Overview
  details: An overview of CSS History, its mission and its core principles.
- title: Additional References
  details: Each chapter is contains link and references for additional information.
- title: Include exercises
  details: Handpicked exercises included at the end of every chapter.
footer: Made by Alexandre Boulay with ❤️
---
