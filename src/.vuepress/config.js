const { description } = require("../../package");

module.exports = {
  base: "/geekfest-css/",
  dest: "public",
  head: [
    ["meta", { name: "theme-color", content: "#03e3fc" }],
    ["meta", { name: "apple-mobile-web-app-capable", content: "yes" }],
    [
      "meta",
      { name: "apple-mobile-web-app-status-bar-style", content: "black" },
    ],
  ],
  locales: {
    "/": {
      lang: "en-CA",
      title: "Styling the Web - CSS Basics",
    },
    "/fr/": {
      lang: "fr-CA",
      title: "Styliser le Web - Les Bases du CSS",
    },
  },

  themeConfig: {
    locales: {
      "/": {
        selectText: "EN",
        label: "EN",
        editLinks: false,
        lastUpdated: false,
        nav: [
          {
            text: "Workshop",
            link: "/workshop/",
          },
          {
            text: "References",
            link: "/references/",
          },
          {
            text: "Gitlab",
            link: "https://gitlab.com/Straktor/geekfest-css",
          },
        ],
        sidebar: {
          "/workshop/": [
            {
              title: "Workshop",
              collapsable: false,
              children: [
                "",
                "01-syntax",
                "02-specificity-inheritance",
                "03-box-model",
                "04-units",
                "05-flexbox",
                "06-whats-next",
              ],
            },
          ],
        },
      },
      "/fr/": {
        selectText: "FR",
        label: "FR",
        editLinks: false,
        lastUpdated: false,
        nav: [
          {
            text: "Atelier",
            link: "/fr/atelier/",
          },
          {
            text: "References",
            link: "/fr/references/",
          },
          {
            text: "Gitlab",
            link: "https://gitlab.com/Straktor/geekfest-css",
          },
        ],
        sidebar: {
          "/fr/atelier/": [
            {
              title: "Atelier",
              collapsable: false,
              children: [
                "",
                "01-syntax",
                "02-specificity-inheritance",
                "03-box-model",
                "04-units",
                "05-flexbox",
                "06-whats-next",
              ],
            },
          ],
        },
      },
    },
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: ["@vuepress/plugin-back-to-top", "@vuepress/plugin-medium-zoom"],
};
