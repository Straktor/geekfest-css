# References

## [Modzilla Developer Network (MDN)](https://developer.mozilla.org/en-US/docs/Web/CSS)
![MDN](../assets/references/MDN.png)

## [CSS Tricks](https://css-tricks.com/)
![CSS Tricks](../assets/references/css-tricks.png)

## [Kevin Powell](https://www.kevinpowell.co/)
![Kevin Powell](../assets/references/kevinPowell.jpeg)


## [Miriam Suzanne](https://www.miriamsuzanne.com/)
![Miriam Suzanne](../assets/references/MiriamSuzanne.png)

## [nana](https://nanajeon.com/) & [Ryan Yu](https://ryanyu.com/)
![CSS Selector Cheetsheet](../assets/01-syntax/exercise.png)

