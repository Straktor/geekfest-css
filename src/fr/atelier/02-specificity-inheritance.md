# Spécificité

La [spécificité](https://developer.mozilla.org/en-US/docs/Web/CSS/Specificity) est le moyen par lequel les navigateurs décident quelles valeurs de propriétés CSS sont les plus pertinentes pour un élément et seront donc appliquées. La spécificité est basée sur les règles de correspondance qui sont composées de différents types de sélecteurs CSS.

## Calcul de la spécificité 

La liste suivante de types de sélecteurs augmente en fonction de la spécificité:
- [Sélecteurs de type](https://developer.mozilla.org/en-US/docs/Web/CSS/Type_selectors)
- [Sélecteurs de classe](https://developer.mozilla.org/en-US/docs/Web/CSS/Class_selectors)
- [Sélecteurs d'ID](https://developer.mozilla.org/en-US/docs/Web/CSS/ID_selectors)


| Sélecteurs           | En ligne | ID  | Classe | Type | Spécificité totale |
| -------------------- | -------- | --- | ------ | ---- | ------------------ |
| h1                   | 0        | 0   | 0      | 1    | 0001               |
| h1 > p::first-letter | 0        | 0   | 0      | 3    | 0003               |
| .container           | 0        | 0   | 1      | 0    | 0010               |
| #identifier          | 0        | 1   | 0      | 0    | 0100               |
| #outer #inner a      | 0        | 2   | 0      | 1    | 0201               |
| Inline CSS           | 1        | 0   | 0      | 0    | 1000               |

Il existe de nombreux [calculateurs de spécificité](https://specificity.keegan.st/) pour aider à comprendre visuellement la spécificité des CSS.

## Inheritance

En CSS, [l'héritage](https://developer.mozilla.org/en-US/docs/Web/CSS/inheritance) contrôle ce qui se passe lorsqu'aucune valeur n'est spécifiée pour une propriété d'un élément.

Les propriétés CSS peuvent être classées en deux catégories :

- **les propriétés héritées**
- **propriétés non héritées**

### Inherited properties
Un exemple typique de propriété héritée est la propriété [color](https://developer.mozilla.org/en-US/docs/Web/CSS/color)

```CSS
p { color: green; }
```

```html
<p>Ce paragraphe contient du <em>texte mis en valeur</em></p>.
```

Les mots "texte mis en valeur" apparaîtront en vert, puisque l'élément `em` a hérité de la valeur de la propriété color de l'élément `p`.

### Non-inherited properties
Un exemple typique de propriété non héritée est la propriété [border](https://developer.mozilla.org/en-US/docs/Web/CSS/border)

```CSS
p { border: medium solid; }
```

```html
<p>Ce paragraphe contient du <em>texte mis en valeur</em></p>
```

les mots "texte mis en valeur" n'auront pas de bordure

## Exercice

Familiarisons-nous avec la spécificité.

<Codepen
    title="Box Model"
    user="Straktor"
    slug="GREPgjM"
    :css="false"
/>