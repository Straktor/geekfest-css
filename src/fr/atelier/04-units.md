# Unités

## Unités absolue

Les [unités](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Values_and_units#lengths) les plus populaires lorsque l'on débute en CSS sont généralement les pixels. Elles sont simple à comprendre, mais elles n'offrent pas de flexibilité lorsque le canevas change.

| Unité | Nom    |
| ----- | ------ |
| px    | Pixels |
| ...   |        |

## Unités relative

Les [unités relatives](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Values_and_units#lengths) sont un excellent moyen de définir vos composants de manière à permettre au navigateur d'adapter votre conception en fonction du canevas de l'utilisateur.

| Unité | Relative à                                                                                                                                                                                         |
| ----- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| em    | Pour les propriétés typographiques comme font-size, relative à la taille de la police de l'élément parent. Pour les autres propriétés comme width, relative à la taille de la police de l'élément; |
| ex    | La hauteur d'x de la police de l'élément.                                                                                                                                                          |
| ch    | La chasse/avance du glyphe « 0 » pour la police de l'élément.                                                                                                                                      |
| rem   | La taille de la police pour l'élément racine.                                                                                                                                                      |
| lh    | La hauteur de ligne pour l'élément.                                                                                                                                                                |
| vw    | 1% de la largeur du viewport (la zone d'affichage).                                                                                                                                                |
| vh    | 1% de la hauteur du viewport (la zone d'affichage).                                                                                                                                                |
| vmin  | 1% de la plus petite dimension du viewport (la zone d'affichage).                                                                                                                                  |
| vmax  | 1% de la plus grande dimension du viewport (la zone d'affichage).                                                                                                                                  |

## Exercice

Familiarisons-nous avec les différentes unités disponibles.

<Codepen
    title="Units"
    user="Straktor"
    slug="powQYRa"
    :css="false"
/>

