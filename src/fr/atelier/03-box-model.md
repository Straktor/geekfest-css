# Modèle de boîte

Lors de la mise en page d'un document, le moteur de rendu du navigateur représente chaque élément sous la forme d'une boîte rectangulaire conformément à la norme [CSS basic box model](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Model/Introduction_to_the_CSS_box_model).

## Parties d'une boîte

Chaque boîte est composée de quatre parties (ou zones) :

- **Boîte de contenu** : La zone où votre contenu est affiché.

- **Boîte de remplissage** : L'espace de remplissage se trouve autour du contenu et sert d'espace blanc.

- **Boîte de bordure** : La zone de bordure entoure le contenu et tout le rembourrage.

- **Boîte de marge** : La marge est la couche la plus externe, qui enveloppe le contenu, le rembourrage et la bordure en tant qu'espace blanc entre cette boîte et d'autres éléments.

Le diagramme ci-dessous illustre ces couches :

![boxModel](../../assets/03-boxModel/boxModel.png)

## Exercice

Familiarisons-nous avec le modèle de boîte CSS

<Codepen
    title="Box Model"
    user="carolineartz"
    slug="ogVXZj"
    :css="false"
/>
