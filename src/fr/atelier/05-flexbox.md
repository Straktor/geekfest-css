# Flexbox

[Flexbox](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Flexbox) est une méthode de mise en page unidimensionnelle permettant d'organiser les éléments en lignes ou en colonnes.

[CSS Tricks](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) une excellente documentation visuelle sur Flexbox.

```CSS
.container {
    display: flex;
}
```
### Conteneur
| Propriété       | Description                                                                 |
| --------------- | --------------------------------------------------------------------------- |
| display         | Permet la flexibilité pour tous les enfants                                 |
| flex-direction  | Établit l'axe principal                                                     |
| flex-wrap       | Enveloppe les éléments s'ils ne peuvent pas tous tenir sur une seule ligne. |
| justify-content | Répartition de l'espace supplémentaire sur l'axe principal                  |
| align-items     | Détermine la façon dont les éléments sont disposés sur l'axe transversal    |

### Enfant
| Propriété   | Description                                                                          |
| ----------- | ------------------------------------------------------------------------------------ |
| order       | définir l'ordre dans lequel vous souhaitez que chaque enfant apparaisse              |
| flex-grow   | déterminer comment chaque enfant est autorisé à grandir en tant que partie d'un tout |
| flex-basis  | taille d'un élément avant que l'espace restant ne soit distribué                     |
| flex-shrink | déterminer comment chaque enfant est autorisé à grandir en tant que partie d'un tout |
| align-self  | définit l'alignement d'un élément individuel                                         |


## Exercise

Familiarisons-nous avec Flexbox.

<Codepen
    title="Flexbox"
    user="Straktor"
    slug="VwWqLXV"
    :css="false"
/>
