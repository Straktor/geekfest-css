# Introduction

La séparation de la structure du document de sa mise en page était un objectif du HTML depuis sa création en 1990.

La structure d'un document est définie par un [Markup Language](https://en.wikipedia.org/wiki/Markup_language) tel que HTML, XHTML et XML.

La présentation d'un document est définie par des feuilles de style tel que CSS.

## La vie avant les feuilles de style

Voici à quoi ressemblait l'internet autrefois ([Emulateur du premier navigateur](https://line-mode.cern.ch/www/hypertext/WWW/TheProject.html))


![Emulateur](../../assets/00-introduction/emulator.jpg)

## History by Dates
| Version  | Date       | Description                                                                                                                             |
| -------- | ---------- | --------------------------------------------------------------------------------------------------------------------------------------- |
| HTML 1.0 | 1993       | Intention de partager des informations qui peuvent être lues et accessibles via des navigateurs web                                     |
| HTML 2.0 | 1995       | Contient toutes les caractéristiques de HTML 1.0 ainsi que quelques caractéristiques supplémentaires.                                   |
| CSS 1    | 1996       | Des caractéristiques plus puissantes pour les webmasters dans la conception des pages web                                               |
| HTML 3.0 | 1997       | Nouvelles fonctionnalités, offrant des caractéristiques plus puissantes aux webmasters. Ralentissement du navigateur                    |
| CSS 2    | 1998       | Essentiellement un surensemble de CSS 1, il souffre d'un certain nombre d'erreurs                                                       |
| HTML 4.0 | 1999       | Largement utilisé et très performant                                                                                                    |
| CSS 3    | 1999-Today | Au lieu d'une spécification unique et monolithique, elle a été publiée sous forme de [modules](https://en.wikipedia.org/wiki/CSS#CSS_3) |
| CSS 2.1  | 2011       | Correction des erreurs trouvées dans CSS                                                                                                |
| HTML 5.0 | 2014       | Utilisé dans le monde entier. Version étendue de HTML 4.00                                                                              |


## CSS Mission
CSS est le langage qui décrit la présentation des pages web, y compris les couleurs, la mise en page et les polices de caractères.
Il permet d'adapter la présentation à différents types d'appareils, tels que les grands écrans, les petits écrans ou les imprimantes.

La séparation entre HTML et CSS facilite la maintenance des sites, le partage des feuilles de style entre les pages et l'adaptation des pages à différents environnements.
([w3.org](https://www.w3.org/standards/webdesign/htmlcss.html))
  
## Cascading Style Sheets

L'algorithme de la cascade CSS consiste à sélectionner des déclarations CSS afin de déterminer les valeurs correctes des propriétés CSS. Les déclarations CSS proviennent de différents types d'origine : les [feuilles de style de l'agent utilisateur](https://developer.mozilla.org/en-US/docs/Web/CSS/Cascade#user-agent_stylesheets), les [feuilles de style du site](https://developer.mozilla.org/en-US/docs/Web/CSS/Cascade#author_stylesheets) et les [feuilles de style de l'utilisatrice ou l'utilisateur](https://developer.mozilla.org/en-US/docs/Web/CSS/Cascade#user_stylesheets).

Bien que les feuilles de style proviennent de ces différentes origines et que chacune d'entre elles peut avoir différentes couches, leurs portées se chevauchent. Pour que l'ensemble fonctionne, l'algorithme de la cascade définit comment elles interagissent.

![cssCascade](../../assets/00-introduction/cssCascade.jpg)

## Surdéfinir

Sachant que le CSS résultant est une cascade de **feuille de style de l'utilisateur**, **feuille de style de l'auteur** et **feuille de style du système**, il est important que lorsque nous écrivons du CSS, nous essayions de ne pas surdéfinir chaque propriété.  Nous devrions "suggérer" au navigateur la façon dont nous voudrions qu'il s'affiche.  Cela permet généralement de prendre en charge un plus grand nombre de clients (téléphone intelligent, courrier électronique, navigateurs...).

![designControlSuggest](../../assets/00-introduction/designControlSuggest.png)

Its important to remember that the default CSS properties ensure that all the content of the markup language is shown and that the page is responsive.  This means that when a webpage stops working on a certain device it is usually due to some CSS properties that were added.

## Exercice 

Essayez de jouer avec la propriété overflow

```css
overflow: visible|hidden|scroll|auto|initial|inherit;
```

Vous pouvez également essayer de modifier ou de supprimer la largeur

<Codepen
    title="CSS IS AWESOME"
    user="miriamsuzanne"
    slug="JgKXeL"
    :css="false"
/>

> Cet exercice est tiré de ce [vidéo](https://www.youtube.com/watch?v=aHUtMbJw8iA) de [Miriam Suzanne](https://twitter.com/TerribleMia) il explique très bien les défis posés par les CSS.
