# Syntax

Pour écrire du CSS, il est important de comprendre la [syntaxe](https://developer.mozilla.org/en-US/docs/Web/CSS/Syntax) et sa structure.

```css
sélecteur {
    propriété: valeur;
}
```

## CSS declaration
- Une paire de propriétés et de valeurs est appelée déclaration.
- Les propriétés et les valeurs sont insensibles à la casse
- Chaque sélecteur est séparé par des `,`

![cssDeclaration](../../assets/01-syntax/cssDeclaration.png)
![groupOfSelectors](../../assets/01-syntax/groupOfSelectors.png)


## Properties

Les propriétés sont des identifiants, c'est-à-dire des noms lisibles par les humains, qui définissent une caractéristique (bordures, marges, couleurs...) d'un élément. Il existe plus de [100 propriétés différentes](https://developer.mozilla.org/en-US/docs/Web/CSS/Reference) en CSS et un nombre presque infini de valeurs différentes.

### List of properties

| Property                                                                              | Description                                                                                                                     | Example                                  |
| ------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------- |
| [font](https://developer.mozilla.org/en-US/docs/Web/CSS/font)                         | [propriété abrégée](https://developer.mozilla.org/en-US/docs/Web/CSS/Shorthand_properties) pour définir la police d'un élément  | `font: italic 1.2em "Fira Sans", serif;` |
| [color](https://developer.mozilla.org/en-US/docs/Web/CSS/color)                       | définit la couleur d'avant-plan                                                                                                 | `color: red;`                            |
| [background-color](https://developer.mozilla.org/en-US/docs/Web/CSS/background-color) | définit la couleur d'arrière-plan d'un élément                                                                                  | `background-color: blue;`                |
| [margin](https://developer.mozilla.org/en-US/docs/Web/CSS/margin)                     | fixer la marge d'un élément                                                                                                     | `margin: 1em`                            |
| [border](https://developer.mozilla.org/en-US/docs/Web/CSS/border)                     | [propriété abrégée](https://developer.mozilla.org/en-US/docs/Web/CSS/Shorthand_properties) pour définir la bordure d'un élément | `border: 1px dashed black;`              |
| [padding](https://developer.mozilla.org/en-US/docs/Web/CSS/padding)                   | définir le remplissage d'un élément                                                                                             | `padding: 1em;`                          |
| [...](https://developer.mozilla.org/en-US/docs/Web/CSS/Reference)                     |                                                                                                                                 |                                          |

## Selectors
[CSS selectors](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors) define the elements that a CSS declaration block impacts.

| Selector                                                              | Characters    | Example           |
| --------------------------------------------------------------------- | ------------- | ----------------- |
| Universel                                                             | `*`           | `*`               |
| Type                                                                  | `elementname` | `p`               |
| Classe                                                                | `.classname`  | `.title`          |
| Id                                                                    | `#id`         | `#title`          |
| Liste                                                                 | `,`           | `h1, h2`          |
| Sélecteurs descendant                                                 | ` `(space)    | `p span`          |
| Sélecteurs enfant                                                     | `>`           | `p > span`        |
| Pseudo classe                                                         | `:`           | `h1:hover`        |
| Pseudo élement                                                        | `::`          | `p::first-letter` |
| [...](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors) |               |                   |

## Exercise

Testez vos compétences avec le [CSS selector cheatsheet](https://frontend30.com/css-selectors-cheatsheet) créé par [nana](https://nanajeon.com/) et [Ryan Yu](https://ryanyu.com/).


![Exercice](../../assets/01-syntax/exercise.png)

Une fois terminé, téléchargez le [CSS selector cheetsheet](https://www.dropbox.com/s/h2hni9o1m1di989/CSS%20selectors%20cheatsheet.pdf?dl=1) pour référence ultérieure.