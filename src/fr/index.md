---
home: true
heroImage: /geekfest.png
tagline: 
actionText: Débuter →
actionLink: /fr/atelier/
features:
- title: Aperçu du CSS
  details: Un aperçu de l'histoire de la CSS, de sa mission et de ses principes fondamentaux.
- title: Références additionnelles
  details: Chaque chapitre contient des liens et des références pour des informations complémentaires.
- title: Avec des exercises
  details: Des exercices à la fin de chaque chapitre.
footer: Fait par Alexandre Boulay avec ❤️
---
